// search_int_set.h

#ifndef _SEARCH_INT_SET_H_
#define _SEARCH_INT_SET_H_

#include "simple_int_set.h"
#include "simple_int_set.cc"

int SearchSimpleIntSet(const SimpleIntSet& s, int val);

#endif  // _SEARCH_INT_SET_H_
