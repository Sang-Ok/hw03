// search_int_set.cc
#ifndef _SEARCH_INT_SET_CC_
#define _SEARCH_INT_SET_CC_


#include "search_int_set.h"


int SearchSimpleIntSet(const SimpleIntSet& s, int val){
	const int* values= s.values();
	const int size	= s.size();
	int head=0,tail=size;
	int ret=(head+tail)/2;
	while(values[ret]!=val){
		if(values[ret] < val)
			head=ret;
		else if(values[ret] > val)
			tail=ret;
		ret=(head+tail)/2;
	}

	return ret;
}



#endif  // _SEARCH_INT_SET_CC_
