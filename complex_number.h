// complex_number.h

#ifndef _COMPLEX_NUMBER_H_
#define _COMPLEX_NUMBER_H_

class ComplexNumber {
 public:
  ComplexNumber();
  ComplexNumber(const ComplexNumber& c);
  ComplexNumber(double real, double imag);

  double real() const;
  double imag() const;
  void Set(double real, double imag);
  void Copy(const ComplexNumber& c);

  ComplexNumber Add(const ComplexNumber& c) const;
  ComplexNumber Subtract(const ComplexNumber& c) const;
  ComplexNumber Multiply(const ComplexNumber& c) const;
  ComplexNumber Divide(const ComplexNumber& c) const;

 private:
  double real_, imag_;
};

#endif  // _COMPLEX_NUMBER_H_
