# Makefile
all: complex_number string_array simple_int_set search_int_set

complex_number: complex_number.h complex_number.cc complex_number_main.cc
        g++ -o complex_number complex_number.cc complex_number_main.cc

string_array: string_array.h string_array.cc string_array_main.cc
        g++ -o string_array string_array.cc string_array_main.cc

simple_int_set: simple_int_set.h simple_int_set.cc simple_int_set_main.cc
        g++ -o simple_int_set simple_int_set.cc simple_int_set_main.cc

search_int_set: search_int_set.h search_int_set.cc search_int_set_main.cc
        g++ -o search_int_set search_int_set.cc search_int_set_main.cc
