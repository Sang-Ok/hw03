// complex_number.cc

#include "complex_number.h"
ComplexNumber::ComplexNumber()				{real_=imag_=0.0;}
ComplexNumber::ComplexNumber(const ComplexNumber& c)	{real_=c.real_;	imag_=c.imag_;}
ComplexNumber::ComplexNumber(double real, double imag)	{real_=real;	imag_=imag;}

double ComplexNumber::real() const{return real_;}
double ComplexNumber::imag() const{return imag_;}
void ComplexNumber::Set(double real, double imag){real_=real;	imag_=imag;}
void ComplexNumber::Copy(const ComplexNumber& c){real_=c.real_;	imag_=c.imag_;}

ComplexNumber ComplexNumber::Add(const ComplexNumber& c) const		{return ComplexNumber( real_+c.real_, imag_+c.imag_);}
ComplexNumber ComplexNumber::Subtract(const ComplexNumber& c) const	{return ComplexNumber( real_-c.real_, imag_-c.imag_);}
ComplexNumber ComplexNumber::Multiply(const ComplexNumber& c) const{
	return ComplexNumber( real_*c.real_ - imag_*c.imag_, real_*c.imag_ + imag_*c.real_);}
ComplexNumber ComplexNumber::Divide(const ComplexNumber& c) const{
	return ComplexNumber( (real_*c.real_ + imag_*c.imag_)/(c.real_*c.real_ + c.imag_*c.imag_),
			      (imag_*c.real_ - real_*c.imag_)/(c.real_*c.real_ + c.imag_*c.imag_)  );}
